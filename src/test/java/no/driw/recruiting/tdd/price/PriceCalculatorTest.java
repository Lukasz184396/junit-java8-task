package no.driw.recruiting.tdd.price;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

public class PriceCalculatorTest {

    /* In the future instead of currency this  standard
     I will create own class where will be course to PLN
    and everyting will be converted to one currency to avoid this currency eception
    and maybe the output of calc to do as a list of prices in each currency
    where was and next in  PLN whole,  so if list would has
    2 elements in USD and 3 in EUR so I will sum them and return additionally the result of sum 5 in PLN

    I suppose that this kind of solution woukd be more forward-looking beacuse now is problem with currencies */

    //mayby I will use lombok to redue Boilerplate Code

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void calculatePriceSuccessWhenTwoUnitAt25Nok() {
        //given
        Unit unit1 = new Unit("Candy", new Price(BigDecimal.valueOf(25), Currency.getInstance("NOK")));
        Unit unit2 = new Unit("Chocolate", new Price(BigDecimal.valueOf(25), Currency.getInstance("NOK")));
        List<Unit> units = Arrays.asList(unit1, unit2);

        //when
        Price price = new PriceCalculator().calculatePrice(units);

        //then
        Price expectedPrice = new Price(BigDecimal.valueOf(50), Currency.getInstance("NOK"));
        Assert.assertEquals(expectedPrice, price);
    }

    @Test
    public void calculatePriceSuccessWhenTwoUnitsAt25NokAndABundleAt100Nok() {
        //given
        Unit unit1 = new Unit("Candy", new Price(BigDecimal.valueOf(25), Currency.getInstance("NOK")));
        Unit unit2 = unit1;
        Bundle bundle = new Bundle(unit1, 6, new Price(BigDecimal.valueOf(100), Currency.getInstance(("NOK"))));
        List<Priced> priceds = Arrays.asList(unit1, unit2, bundle);

        //when
        Price price = new PriceCalculator().calculatePrice(priceds);

        //then
        Price expectedPrice = new Price(BigDecimal.valueOf(150), Currency.getInstance("NOK"));
        Assert.assertEquals(expectedPrice, price);
    }

    @Test
    public void calculatePriceFailureWhenParameterIsNull() {
        //given
        List<Priced> priceds = null;
        expectedException.expect(AssertionError.class);

        //when
        Price price = new PriceCalculator().calculatePrice(priceds);

        //then
        Assert.fail("AssertionError expected");
    }

    @Test
    public void calculatePriceFailureWhenParameterListIsEmpty() {
        //given
        List<Priced> priceds = new ArrayList<>();
        expectedException.expect(AssertionError.class);

        //when
        Price price = new PriceCalculator().calculatePrice(priceds);

        //then
        Assert.fail("AssertionError expected");
    }

    @Test
    public void calculatePriceFailureWhenDifferentCurrencies() {
        //given
        Unit unit1 = new Unit("Candy", new Price(BigDecimal.valueOf(25), Currency.getInstance("NOK")));
        Unit unit2 = new Unit("Chocolate", new Price(BigDecimal.valueOf(25), Currency.getInstance("PLN")));
        List<Unit> units = Arrays.asList(unit1, unit2);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Different currencies in list of items");

        //when
        Price price = new PriceCalculator().calculatePrice(units);

        //then
        Assert.fail("AssertionError expected");
    }

}