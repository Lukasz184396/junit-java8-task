package no.driw.recruiting.tdd.price;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

public class PriceCalculator {
    public Price calculatePrice(List<? extends Priced> items) {
        assert items != null;
        assert items.size() > 0;

        Currency currency = findCurrency(items);
        BigDecimal price = items.stream()
                .map(item -> item.getPrice().getAmount())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return new Price(price, currency);
    }

    private Currency findCurrency(List<? extends Priced> items) {
        List<Currency> currencies = items.stream()
                .map(item -> item.getPrice().getCurrency())
                .distinct()
                .collect(Collectors.toList());
        if(currencies.size() != 1){
            throw new IllegalArgumentException("Different currencies in list of items");
        }
        return currencies.get(0);
    }
}