package no.driw.recruiting.tdd.price;

public class Bundle implements Priced {

    private Unit unit;
    private int units;
    private Price price;

    @Override
    public Price getPrice() {
        return price;
    }

    @Override
    public String getName() {
        return unit.getName();
    }

    public Bundle(Unit unit, int units, Price price) {
        this.unit = unit;
        this.units = units;
        this.price = price;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
}