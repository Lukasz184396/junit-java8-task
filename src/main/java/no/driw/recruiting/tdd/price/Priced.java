package no.driw.recruiting.tdd.price;

public interface Priced {

    Price getPrice();

    String getName();


}
