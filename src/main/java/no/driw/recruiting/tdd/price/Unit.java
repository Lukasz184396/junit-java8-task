package no.driw.recruiting.tdd.price;

public class Unit implements Priced {

    private String name;
    private Price price;

    @Override
    public Price getPrice() {
        return price;
    }

    @Override
    public String getName() {
        return name;
    }

    public Unit() {
    }

    public Unit(String name, Price price) {
        this.name = name;
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
}