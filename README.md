# Short assignment
The estimated completion time of this assignment for an experienced developer familiar with the given tools is estimated
to be 15-30 minutes. For a less experienced developer who is familiar with Java and its paradigms, the estimated 
completion time is 30-60 minutes.

Some parts of this assignment are straight forward, others may require critical thinking. Evaluation will consider how 
you employ standard practices in Java, as well as how you handle basic software architecture and patterns.

## Introduction
The project attempts to implement a simple price calculator for unspecified wares. However, the code is intentionally 
poorly written. Part of the assignment is to improve this code as you believe it should be written.

## Tasks

### Unit testing

The current unit tests are written as an executable program. This is not the desired way to write unit tests in a Gradle
project.

**Convert the unit tests to JUnit, the industry standard in Java testing.**

### Pricing

The system currently handles pricing using simple strings. There are many reasons this is not desired.

**Implement a simple pricing system that is easier to handle arithmetically.**
Keep in mind the earlier assertion that this is a short assignment. We don't expect a full blown currency scheme. Feel 
free to note any drawbacks to your implementation, or tools you feel could improve your implementation.

### Code Quality
As stated above, this code is intentionally poor.

**Refactor or convert code that you feel is poor.**
You're free to employ paradigms you feel are strong for this type of code. If the paradigms you employ deviate from 
established practices in Java, please note them.